package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import cuentas.back.general.modelo.Marca;

@Entity
@Table(name = "ASOCIADO", catalog = "CCUENTAS")
public class Asociados implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ASOCIADO_ID", unique = true, nullable = false)
	private Integer asociadoId;

	@Column(name="NOMBRE")
	private String nombre;

	@Column(name="FECHORACT")
	private Date fechoract;

	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	@Column(name="ESTATUS")
	private Integer estatus;

	@Column(name="CLAVEJDE")
	private String claveJde;
	
	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;

	@Transient
	private String descEstatus;
	
	public String getClaveJde() {
		return claveJde;
	}
	
	public void setClaveJde(String claveJde) {
		this.claveJde = claveJde;
	}

	public Integer getAsociadoId() {
		return asociadoId;
	}

	public void setAsociadoId(Integer asociadoId) {
		this.asociadoId = asociadoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechoract() {
		return fechoract;
	}

	public void setFechoract(Date fechoract) {
		this.fechoract = fechoract;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public String getDescEstatus() {
		return descEstatus;
	}

	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}
}
