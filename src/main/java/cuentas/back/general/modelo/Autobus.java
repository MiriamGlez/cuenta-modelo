package cuentas.back.general.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AUTOBUS", catalog = "GENERAL")
public class Autobus implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "AUTOBUS_ID")
	private Integer idAutobus;

	@Column(name = "NUMECONOMICO")
	private String numeroEconomico;

	@Column(name = "STATUS")
	private Integer estatus;
	
	@Column(name="PLACAS")
	private String placa;

	@Column(name="MARCA_ID")
	private Integer marcaId;
	
	public Integer getIdAutobus() {
		return idAutobus;
	}

	public void setIdAutobus(Integer idAutobus) {
		this.idAutobus = idAutobus;
	}

	public String getNumeroEconomico() {
		return numeroEconomico;
	}

	public void setNumeroEconomico(String numeroEconomico) {
		this.numeroEconomico = numeroEconomico;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void seEstatus(Integer status) {
		this.estatus = status;
	}

//	@Override
//	public Integer getId() {
//		return null;
//	}
//
//	@Override
//	public void setId(Integer id) {
//	}
//
//	@Override
//	public void setClave(String clave) {
//		this.setNumeroEconomico(clave);
//	}
//
//	@Override
//	public String getClave() {
//		return this.getNumeroEconomico();
//	}
//
//	@Override
//	public String getDescripcion() {
//		return this.getPlaca();
//	}
//
//	@Override
//	public void setDescripcion(String newDescripcion) {
//		setPlaca(newDescripcion);
//	}
//
//	@Override
//	public void getQueryPorId() {
//	}
//
//	@Override
//	public void getQueryPorClave() {
//		String query = "Select c from recaudacion.bean.entidades.general.Autobus c where numeroEconomico like ? and estatus = 1 ";
//		if(getMarcaId() != null){
//			query += "and marcaId = ? ";
//		}
//		query += "order by idAutobus";
//		setQueryGenerico(query);
//		setParametros(new Vector<Object>());
//		getParametros().add("%"+getNumeroEconomico().trim()+"%");
//		if(getMarcaId() != null){
//			getParametros().add(getMarcaId());
//		}
//	}
//
//	@Override
//	public void getQueryPorDescripcion() {
//		String query = "Select c from recaudacion.bean.entidades.general.Autobus c where numeroEconomico like ? and estatus = 1 ";
//		if(getMarcaId() != null){
//			query += "and marcaId = ? ";
//		}
//		query += "order by idAutobus";
//		setQueryGenerico(query);
//		setParametros(new Vector<Object>());
//		getParametros().add("%"+getPlaca().trim()+"%");
//		if(getMarcaId() != null){
//			getParametros().add(getMarcaId());
//		}
//	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}
}
