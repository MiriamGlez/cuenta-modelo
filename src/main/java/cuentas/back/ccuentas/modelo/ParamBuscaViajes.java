package cuentas.back.ccuentas.modelo;

import java.io.Serializable;

public class ParamBuscaViajes implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer marcaId;
	
	private Integer marcaRolId;
	
//	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private String fecha;
	
	public Integer getMarcaId() {
		return marcaId;
	}
	
	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}
	
	public Integer getMarcaRolId() {
		return marcaRolId;
	}
	
	public void setMarcaRolId(Integer marcaRolId) {
		this.marcaRolId = marcaRolId;
	}
	
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
