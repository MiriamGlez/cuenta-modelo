package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

public class ParametrosOferta implements Serializable{
	private static final long serialVersionUID = 1L;

	private Integer idMarca;
	private Integer cuadro;
	private Integer numMaxViajes;
	private String tipoOferta;
	private Date horaSalida;
	private Date horaLlegada;
	private Integer idOrigen;
	private String cveOrigen;
	private String descOrigen;	
	private Integer idDestino;
	private String  cveDestino;
	private String  descDestino;
	private Integer frecuencia;
	private String kms;
	private Integer idVia;
	private String via;
	private String aplicaL;
	private String aplicaM;
	private String aplicaMI;
	private String aplicaJ;
	private String aplicaV;
	private String aplicaS;
	private String aplicaD;
	private String numDias;
	private String ValorL;
	private String ValorM;
	private String ValorMI;
	private String ValorJ;
	private String ValorV;
	private String ValorS;
	private String ValorD;
	
	public Integer getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}
	public Integer getCuadro() {
		return cuadro;
	}
	public void setCuadro(Integer cuadro) {
		this.cuadro = cuadro;
	}
	public Date getHoraSalida() {
		return horaSalida;
	}
	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}
	public Date getHoraLlegada() {
		return horaLlegada;
	}
	public void setHoraLlegada(Date horaLlegada) {
		this.horaLlegada = horaLlegada;
	}
	public String getKms() {
		return kms;
	}
	public void setKms(String kms) {
		this.kms = kms;
	}
	public Integer getIdVia() {
		return idVia;
	}
	public void setIdVia(Integer idVia) {
		this.idVia = idVia;
	}
	public String getAplicaL() {
		return aplicaL;
	}
	public void setAplicaL(String aplicaL) {
		this.aplicaL = aplicaL;
	}
	public String getAplicaM() {
		return aplicaM;
	}
	public void setAplicaM(String aplicaM) {
		this.aplicaM = aplicaM;
	}
	public String getAplicaMI() {
		return aplicaMI;
	}
	public void setAplicaMI(String aplicaMI) {
		this.aplicaMI = aplicaMI;
	}
	public String getAplicaJ() {
		return aplicaJ;
	}
	public void setAplicaJ(String aplicaJ) {
		this.aplicaJ = aplicaJ;
	}
	public String getAplicaV() {
		return aplicaV;
	}
	public void setAplicaV(String aplicaV) {
		this.aplicaV = aplicaV;
	}
	public String getAplicaS() {
		return aplicaS;
	}
	public void setAplicaS(String aplicaS) {
		this.aplicaS = aplicaS;
	}
	public String getAplicaD() {
		return aplicaD;
	}
	public void setAplicaD(String aplicaD) {
		this.aplicaD = aplicaD;
	}
	public String getNumDias() {
		return numDias;
	}
	public void setNumDias(String numDias) {
		this.numDias = numDias;
	}
	public String getValorL() {
		return ValorL;
	}
	public void setValorL(String valorL) {
		ValorL = valorL;
	}
	public String getValorM() {
		return ValorM;
	}
	public void setValorM(String valorM) {
		ValorM = valorM;
	}
	public String getValorMI() {
		return ValorMI;
	}
	public void setValorMI(String valorMI) {
		ValorMI = valorMI;
	}
	public String getValorJ() {
		return ValorJ;
	}
	public void setValorJ(String valorJ) {
		ValorJ = valorJ;
	}
	public String getValorV() {
		return ValorV;
	}
	public void setValorV(String valorV) {
		ValorV = valorV;
	}
	public String getValorS() {
		return ValorS;
	}
	public void setValorS(String valorS) {
		ValorS = valorS;
	}
	public String getValorD() {
		return ValorD;
	}
	public void setValorD(String valorD) {
		ValorD = valorD;
	}
	public Integer getNumMaxViajes() {
		return numMaxViajes;
	}
	public void setNumMaxViajes(Integer numMaxViajes) {
		this.numMaxViajes = numMaxViajes;
	}
	public Integer getIdOrigen() {
		return idOrigen;
	}
	public void setIdOrigen(Integer idOrigen) {
		this.idOrigen = idOrigen;
	}
	public Integer getIdDestino() {
		return idDestino;
	}
	public void setIdDestino(Integer idDestino) {
		this.idDestino = idDestino;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public String getCveOrigen() {
		return cveOrigen;
	}
	public void setCveOrigen(String cveOrigen) {
		this.cveOrigen = cveOrigen;
	}
	public String getDescOrigen() {
		return descOrigen;
	}
	public void setDescOrigen(String descOrigen) {
		this.descOrigen = descOrigen;
	}
	public String getCveDestino() {
		return cveDestino;
	}
	public void setCveDestino(String cveDestino) {
		this.cveDestino = cveDestino;
	}
	public String getDescDestino() {
		return descDestino;
	}
	public void setDescDestino(String descDestino) {
		this.descDestino = descDestino;
	}
	public Integer getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(Integer frecuencia) {
		this.frecuencia = frecuencia;
	}
	public String getTipoOferta() {
		return tipoOferta;
	}
	public void setTipoOferta(String tipoOferta) {
		this.tipoOferta = tipoOferta;
	}
}
