package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

public class ValorOferta implements Serializable{
	private static final long serialVersionUID = 1L;

	private String descripcion;
	private String cuadro;
	private String secuencia;
	private Date horaSalida;
	private String origen;
	private String  destino;
	private String kms;
	private String via;
	private String aplicaL;
	private String aplicaM;
	private String aplicaMI;
	private String aplicaJ;
	private String aplicaV;
	private String aplicaS;
	private String aplicaD;
	private String numDias;
	private String ValorL;
	private String 	ValorM;
	private String ValorMI;
	private String ValorJ;
	private String ValorV;
	private String ValorS;
	private String ValorD;
	private String descMarca;
	private String descMarcaRol;
	private Integer idMarca;
	private Integer idMarcaRol;
	private String tipoOferta;
	
	private Vector<GeneracionCuadros> generaCuadros; 

	public String getCuadro() {
		return cuadro;
	}
	public void setCuadro(String cuadro) {
		this.cuadro = cuadro;
	}
	public String getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	public Date getHoraSalida() {
		return horaSalida;
	}
	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getKms() {
		return kms;
	}
	public void setKms(String kms) {
		this.kms = kms;
	}
	public String getAplicaL() {
		return aplicaL;
	}
	public void setAplicaL(String aplicaL) {
		this.aplicaL = aplicaL;
	}
	public String getAplicaM() {
		return aplicaM;
	}
	public void setAplicaM(String aplicaM) {
		this.aplicaM = aplicaM;
	}
	public String getAplicaMI() {
		return aplicaMI;
	}
	public void setAplicaMI(String aplicaMI) {
		this.aplicaMI = aplicaMI;
	}
	public String getAplicaJ() {
		return aplicaJ;
	}
	public void setAplicaJ(String aplicaJ) {
		this.aplicaJ = aplicaJ;
	}
	public String getAplicaV() {
		return aplicaV;
	}
	public void setAplicaV(String aplicaV) {
		this.aplicaV = aplicaV;
	}
	public String getAplicaS() {
		return aplicaS;
	}
	public void setAplicaS(String aplicaS) {
		this.aplicaS = aplicaS;
	}
	public String getAplicaD() {
		return aplicaD;
	}
	public void setAplicaD(String aplicaD) {
		this.aplicaD = aplicaD;
	}
	public String getNumDias() {
		return numDias;
	}
	public void setNumDias(String numDias) {
		this.numDias = numDias;
	}
	public String getValorL() {
		return ValorL;
	}
	public void setValorL(String valorL) {
		ValorL = valorL;
	}
	public String getValorM() {
		return ValorM;
	}
	public void setValorM(String valorM) {
		ValorM = valorM;
	}
	public String getValorMI() {
		return ValorMI;
	}
	public void setValorMI(String valorMI) {
		ValorMI = valorMI;
	}
	public String getValorJ() {
		return ValorJ;
	}
	public void setValorJ(String valorJ) {
		ValorJ = valorJ;
	}
	public String getValorV() {
		return ValorV;
	}
	public void setValorV(String valorV) {
		ValorV = valorV;
	}
	public String getValorS() {
		return ValorS;
	}
	public void setValorS(String valorS) {
		ValorS = valorS;
	}
	public String getValorD() {
		return ValorD;
	}
	public void setValorD(String valorD) {
		ValorD = valorD;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescMarca() {
		return descMarca;
	}
	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}
	public String getDescMarcaRol() {
		return descMarcaRol;
	}
	public void setDescMarcaRol(String descMarcaRol) {
		this.descMarcaRol = descMarcaRol;
	}
	public Integer getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}
	public Integer getIdMarcaRol() {
		return idMarcaRol;
	}
	public void setIdMarcaRol(Integer idMarcaRol) {
		this.idMarcaRol = idMarcaRol;
	}
	public Vector<GeneracionCuadros> getGeneraCuadros() {
		return generaCuadros;
	}
	public void setGeneraCuadros(Vector<GeneracionCuadros> generaCuadros) {
		this.generaCuadros = generaCuadros;
	}
	public String getTipoOferta() {
		return tipoOferta;
	}
	public void setTipoOferta(String tipoOferta) {
		this.tipoOferta = tipoOferta;
	}
}
