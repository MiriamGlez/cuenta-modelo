package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="PARADA", catalog = "CCUENTAS")
public class Parada implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PARADA_ID")
	private Integer paradaId;
	
	@Column(name="CVEPARADA")
	private String cveParada;
	
	@Column(name="DESCPARADA")
	private String descParada;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="STATUS")
	private Integer status;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;

	@Transient
	private boolean valorEstatus;
	
	@Transient
	private String descEstatus;
	
	public Integer getParadaId() {
		return paradaId;
	}

	public void setParadaId(Integer paradaId) {
		this.paradaId = paradaId;
	}

	public String getCveParada() {
		return cveParada;
	}

	public void setCveParada(String cveParada) {
		this.cveParada = cveParada;
	}

	public String getDescParada() {
		return descParada;
	}

	public void setDescParada(String descParada) {
		this.descParada = descParada;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public boolean isValorEstatus() {
		return valorEstatus;
	}

	public void setValorEstatus(boolean valorEstatus) {
		this.valorEstatus = valorEstatus;
	}

	public String getDescEstatus() {
		return descEstatus;
	}
	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}
}
