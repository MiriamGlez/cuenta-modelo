package cuentas.back.ccuentas.constantes;

public class Constantes
{
	
	public static String MODULO_ARP = "ARP";
	public static String MODULO_SIN_BARRA = "Sin Barra";
	
	public static String dsPool = "erpco/DS/arp/poolcomun";
	
	public final static int ESTATUS_INACTIVO = 0;
	public final static int ESTATUS_ACTIVO = 1;
	public final static int ESTATUS_CTAS_RECAUDADA = 3;
	
	//CONSTANTES PARA DEPOSITOS EN CAMINO 
	public final static int ESTATUS_PENDIENTE = 1;
	public final static int ESTATUS_LIQUIDADO = 2;
	
	// CONSTANTES DE TIPO OFERTA
	public static final int NORMAL = 1;
	public static final int VACIONAL = 2;
	
	//CONSTANTES DIAS APLICACION SEMANA

	public static final int DIA_DOMINGO = 1;
	public static final int DIA_LUNES = 2;
	public static final int DIA_MARTES = 3;
	public static final int DIA_MIERCOLES = 4;
	public static final int DIA_JUEVES = 5;
	public static final int DIA_VIERNES = 6;
	public static final int DIA_SABADO = 7;
	
	//CONSTANTES CELDAS DE EXCEL
	public static final int CELDA_CUADRO = 0;
	public static final int CELDA_SECUENCIA = 1;
	public static final int CELDA_HORASALIDA = 2;
	public static final int CELDA_ORIGEN = 3;
	public static final int CELDA_DESTINO = 4;
	public static final int CELDA_KMS = 5;
	public static final int CELDA_VIA = 6;
	public static final int CELDA_AL = 7;
	public static final int CELDA_AM = 8;
	public static final int CELDA_AW = 9;
	public static final int CELDA_AJ = 10;
	public static final int CELDA_AV = 11;
	public static final int CELDA_AS = 12;
	public static final int CELDA_AD = 13;
	public static final int CELDA_NUMDIAS = 14;
	public static final int CELDA_VL = 15;
	public static final int CELDA_VM = 16;
	public static final int CELDA_VW = 17;
	public static final int CELDA_VJ = 18;
	public static final int CELDA_VV = 19;
	public static final int CELDA_VS = 20;
	public static final int CELDA_VD = 21;
	
	//	CONSTANTES PARA CAMBIOS DE RECURSOS
	
	public static final int CAMBIO_CONDUCTOR = 1;
	public static final int CAMBIO_AUTOBUS = 2;
	public static final int CAMBIO_AUTOBUS_EXTERNO = 3;
	
	// TIPO DE ESTATUS PARA VIAJES Y TARJETAS 
	public static final int ESTATUS_CREADO = 1;					//BLANCO
	public static final int ESTATUS_LIBERADO = 2;  				// VERDE	
	public static final int ESTATUS_CANCELADO= 3;				// ROJO
	public static final int ESTATUS_AJUSTADO = 4;				// AZUL
	public static final int ESTATUS_PENDIENTE_EN_CAMINO = 5;	// CAFE
	public static final int ESTATUS_VIAJE_EXTRA = 6;			// ROSA
	public static final int ESTATUS_RECAUDADO = 7;				// AMARILLO
	
	//ESTATUS PARA EL REGISTRO  GUARDIAS, DESCANSOS Y MANTENIMIENTOS (INCIDENCIAS)
	public static final int ESTATUS_GUARDIA = 1;
	public static final int ESTATUS_DESCANSO = 2;
	public static final int ESTATUS_MANTENIMIENTO = 3;
	
	
	//TIPO CARGO PARA REGISTRO DE COMBUSTIBLE
	public static final int CARGO_VIAJE_RUTA = 1;
	public static final int CARGO_REGISTRO_COMBUSTIBLE = 2; 
}
