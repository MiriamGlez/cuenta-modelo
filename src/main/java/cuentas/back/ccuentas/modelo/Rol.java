package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ROL", catalog="CCUENTAS")
public class Rol implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ROL_ID")
	private Integer rolId;
	
	@Column(name="MARCA_ID")
	private Integer marcaId;
	
	@Column(name="MARCALROL_ID")
	private Integer marcaRolId;
	
	@Column(name="VUELTAS_MAXIMAS")
	private Integer numCuadros;
	
	@Column(name="MEDIAS_VUELTAS")
	private Integer numVueltas;
	
	@Column(name="TIPO_OFERTA")
	private Integer tipoOferta;
	
	@Column(name="PERIODO_INICIAL")
	private Date periodoInicial;
	
	@Column(name="PERIODO_FINAL")
	private Date periodoFinal;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;

	public Integer getRolId() {
		return rolId;
	}

	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}

	public Integer getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Integer marcaId) {
		this.marcaId = marcaId;
	}

	public Integer getMarcaRolId() {
		return marcaRolId;
	}

	public void setMarcaRolId(Integer marcaRolId) {
		this.marcaRolId = marcaRolId;
	}

	public Integer getNumCuadros() {
		return numCuadros;
	}

	public void setNumCuadros(Integer numCuadros) {
		this.numCuadros = numCuadros;
	}

	public Integer getNumVueltas() {
		return numVueltas;
	}

	public void setNumVueltas(Integer numVueltas) {
		this.numVueltas = numVueltas;
	}

	public Integer getTipoOferta() {
		return tipoOferta;
	}

	public void setTipoOferta(Integer tipoOferta) {
		this.tipoOferta = tipoOferta;
	}

	public Date getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(Date periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public Date getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(Date periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}
}