package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DetalleTramoKey implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="TRAMO_ID")
	private Integer tramoId = null;
	
	@Column(name="DIASEMANA")
	private String diaSemana = null;
	
	@Column(name="INICIO")
	private Timestamp inicio = null;

	public Integer getTramoId() {
		return tramoId;
	}
	public void setTramoId(Integer tramoId) {
		this.tramoId = tramoId;
	}
	public String getDiaSemana() {
		return diaSemana;
	}
	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}
	public Timestamp getInicio() {
		return inicio;
	}
	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}
}
