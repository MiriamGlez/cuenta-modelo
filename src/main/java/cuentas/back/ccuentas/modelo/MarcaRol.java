package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Usuario;

@Entity
@Table(name="MARCA_ROL", catalog = "CCUENTAS")
public class MarcaRol implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MARCAROL_ID", unique = true, nullable = false)
	private Integer rolId;
	
	@Column(name="DESC_ROL")
	private String descripcion;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@ManyToOne
	@JoinColumn(name="USUARIO_ID")
	private Usuario usuario;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@Transient
	private String descStatus;
	
	public MarcaRol() {
	}
	
	public MarcaRol(Integer rolId, String descripcion, Marca marca, Date fecHorAct, Usuario usuario, Integer estatus) {
		super();
		this.rolId = rolId;
		this.descripcion = descripcion;
		this.marca = marca;
		this.fecHorAct = fecHorAct;
		this.usuario = usuario;
		this.estatus = estatus;
	}

	public Integer getRolId() {
		return rolId;
	}
	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}
	public Date getFecHorAct() {
		return fecHorAct;
	}
	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDescStatus() {
		return descStatus;
	}

	public void setDescStatus(String descStatus) {
		this.descStatus = descStatus;
	}
}