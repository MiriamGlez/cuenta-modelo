package cuentas.back.general.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REGION", catalog = "GENERAL")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "REGION_ID")
	private Integer id;

	@Column(name = "NOMBREGION")
	public String descRegion;

	@Column(name = "STATUS")
	public Integer status;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescRegion() {
		return descRegion;
	}

	public void setDescRegion(String descRegion) {
		this.descRegion = descRegion;
	}

	public Integer getId() {
		return id;
	}
}
