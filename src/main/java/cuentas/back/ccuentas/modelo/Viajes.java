package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sun.xml.bind.v2.runtime.reflect.opt.Const;

import cuentas.back.ccuentas.constantes.Constantes;
import cuentas.back.general.modelo.Autobus;
import cuentas.back.general.modelo.Empleado;
import cuentas.back.general.modelo.Marca;

@Entity
@Table(name = "VIAJES", catalog = "CCUENTAS")
public class Viajes implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@SequenceGenerator(name="VIAJESID", sequenceName="CCUENTAS.VIAJES_SEQ", allocationSize=1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VIAJESID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "VIAJES_ID")
	private Integer viajeId;

	@ManyToOne
	@JoinColumn(name = "ROL_ID")
	private Rol rol;

	@Column(name = "CUADRO")
	private Integer cuadro;

	@Column(name = "SECUENCIA")
	private String secuencia;

	@Column(name = "HORA_SALIDA")
	private Date horaSalida;

	@ManyToOne
	@JoinColumn(name = "ORIGEN")
	private Parada origen;

	@ManyToOne
	@JoinColumn(name = "DESTINO")
	private Parada destino;

	@Column(name = "KMS")
	private Integer kms;

	@Column(name = "FECHA")
	private Date fecha;

	@Column(name = "DIA_APLICACION")
	private String diaAplicacion;

	@Column(name = "COSTO_VIAJE")
	private Double costoViaje;

	@ManyToOne
	@JoinColumn(name = "AUTOBUS_ID")
	private Autobus autobus;

	@ManyToOne
	@JoinColumn(name = "AUTOBUSEXT_ID")
	private AutobusExterno autobusExterno;

	@ManyToOne
	@JoinColumn(name = "CONDUCTOR_ID")
	private Empleado conductor;

	@Column(name = "STAUS_VIAJE")
	private Integer estatusViaje;

	@Column(name = "FECHORAT")
	private Date fecHorAct;

	@Column(name = "USUARIO_ID")
	private Integer usuarioId;

	@ManyToOne
	@JoinColumn(name = "MARCA_ID")
	private Marca marca;

	@ManyToOne
	@JoinColumn(name = "MARCAROL_ID")
	private MarcaRol marcarol;

	@Column(name = "FECHALLEGADA")
	private Date fechaLlegada;

	@Transient
	private Double costoAjuste;

	@Transient
	private Integer viaId;

	@Transient
	private String descVia;

	@Transient
	private String descEstatus ;

	public Integer getViajeId() {
		return viajeId;
	}

	public void setViajeId(Integer viajeId) {
		this.viajeId = viajeId;
	}

	public Integer getCuadro() {
		return cuadro;
	}

	public void setCuadro(Integer cuadro) {
		this.cuadro = cuadro;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Parada getOrigen() {
		return origen;
	}

	public void setOrigen(Parada origen) {
		this.origen = origen;
	}

	public Parada getDestino() {
		return destino;
	}

	public void setDestino(Parada destino) {
		this.destino = destino;
	}

	public Integer getKms() {
		return kms;
	}

	public void setKms(Integer kms) {
		this.kms = kms;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDiaAplicacion() {
		return diaAplicacion;
	}

	public void setDiaAplicacion(String diaAplicacion) {
		this.diaAplicacion = diaAplicacion;
	}

	public Double getCostoViaje() {
		return costoViaje;
	}

	public void setCostoViaje(Double costoViaje) {
		this.costoViaje = costoViaje;
	}

	public Autobus getAutobus() {
		return autobus;
	}

	public void setAutobus(Autobus autobus) {
		this.autobus = autobus;
	}

	public AutobusExterno getAutobusExterno() {
		return autobusExterno;
	}

	public void setAutobusExterno(AutobusExterno autobusExterno) {
		this.autobusExterno = autobusExterno;
	}

	public Empleado getConductor() {
		return conductor;
	}

	public void setConductor(Empleado conductor) {
		this.conductor = conductor;
	}

	public Integer getEstatusViaje() {
		return estatusViaje;
	}

	public void setEstatusViaje(Integer estatusViaje) {
		this.estatusViaje = estatusViaje;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Double getCostoAjuste() {
		return costoAjuste;
	}

	public void setCostoAjuste(Double costoAjuste) {
		this.costoAjuste = costoAjuste;
	}

	public String getDescVia() {
		return descVia;
	}

	public void setDescVia(String descVia) {
		this.descVia = descVia;
	}

	public Integer getViaId() {
		return viaId;
	}

	public void setViaId(Integer viaId) {
		this.viaId = viaId;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public MarcaRol getMarcarol() {
		return marcarol;
	}

	public void setMarcarol(MarcaRol marcarol) {
		this.marcarol = marcarol;
	}

	public Date getFechaLlegada() {
		return fechaLlegada;
	}

	public void setFechaLlegada(Date fechaLlegada) {
		this.fechaLlegada = fechaLlegada;
	}

	public String getDescEstatus() {
		if(getEstatusViaje() == null) {
			return "";
		}
		if (getEstatusViaje() == Constantes.ESTATUS_CREADO) {
			descEstatus = "CREADO";
		} else if (getEstatusViaje() == Constantes.ESTATUS_LIBERADO) {
			descEstatus = "CONFIRMADO";
		} else if (getEstatusViaje() == Constantes.ESTATUS_CANCELADO) {
			descEstatus ="CANCELADO";
		} else if (getEstatusViaje() == Constantes.ESTATUS_AJUSTADO) {
			descEstatus = "AJUSTADO";
		} else if (getEstatusViaje() == Constantes.ESTATUS_PENDIENTE_EN_CAMINO) {
			descEstatus ="PENDIENTE EN CAMINO";
		} else if (getEstatusViaje() == Constantes.ESTATUS_VIAJE_EXTRA) {
			descEstatus ="ADICIONAL";
		} else if (getEstatusViaje() == Constantes.ESTATUS_RECAUDADO) {
			descEstatus ="RECAUDADO";
		}
		return descEstatus;
	}

	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}

}
