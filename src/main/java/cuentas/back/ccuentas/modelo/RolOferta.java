package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

public class RolOferta implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer idRol;
	private Integer idOrigen;
	private Integer idDestino;
	private Integer idVia;
	
	private Double kms;
	
	private String descRol;
	private String cveOrigen;
	private String cveDestino;
	private String descVia;
	
	private Date horaInicio;
	private Date horaTermina;
	private Date frecuencia;
	private Date tRecorrido;
	
	public Integer getIdRol() {
		return idRol;
	}
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}
	public Integer getIdOrigen() {
		return idOrigen;
	}
	public void setIdOrigen(Integer idOrigen) {
		this.idOrigen = idOrigen;
	}
	public Integer getIdDestino() {
		return idDestino;
	}
	public void setIdDestino(Integer idDestino) {
		this.idDestino = idDestino;
	}
	public Integer getIdVia() {
		return idVia;
	}
	public void setIdVia(Integer idVia) {
		this.idVia = idVia;
	}
	public String getDescRol() {
		return descRol;
	}
	public void setDescRol(String descRol) {
		this.descRol = descRol;
	}
	public String getCveOrigen() {
		return cveOrigen;
	}
	public void setCveOrigen(String cveOrigen) {
		this.cveOrigen = cveOrigen;
	}
	public String getCveDestino() {
		return cveDestino;
	}
	public void setCveDestino(String cveDestino) {
		this.cveDestino = cveDestino;
	}
	public String getDescVia() {
		return descVia;
	}
	public void setDescVia(String descVia) {
		this.descVia = descVia;
	}
	public Date getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}
	public Date getHoraTermina() {
		return horaTermina;
	}
	public void setHoraTermina(Date horaTermina) {
		this.horaTermina = horaTermina;
	}
	public Date getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(Date frecuencia) {
		this.frecuencia = frecuencia;
	}
	public Date gettRecorrido() {
		return tRecorrido;
	}
	public void settRecorrido(Date tRecorrido) {
		this.tRecorrido = tRecorrido;
	}
	public Double getKms() {
		return kms;
	}
	public void setKms(Double kms) {
		this.kms = kms;
	}
}
