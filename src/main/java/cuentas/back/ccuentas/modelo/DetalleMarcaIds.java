package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

public class DetalleMarcaIds implements Serializable{
	private static final long serialVersionUID = 1L;

	private Integer idMarca;
	
	private Integer idMarcaRol;
	
	private Double valorFianza;
	
	private Date fechaFianza;
	
	private Double factorKms;
	
	private Double valorCuenta;
	
	private Integer cuentasPermitidas;
	
	public Double getValorFianza() {
		return valorFianza;
	}

	public void setValorFianza(Double valorFianza) {
		this.valorFianza = valorFianza;
	}

	public Date getFechaFianza() {
		return fechaFianza;
	}

	public void setFechaFianza(Date fechaFianza) {
		this.fechaFianza = fechaFianza;
	}

	public Double getFactorKms() {
		return factorKms;
	}

	public void setFactorKms(Double factorKms) {
		this.factorKms = factorKms;
	}

	public Double getValorCuenta() {
		return valorCuenta;
	}

	public void setValorCuenta(Double valorCuenta) {
		this.valorCuenta = valorCuenta;
	}

	public Integer getCuentasPermitidas() {
		return cuentasPermitidas;
	}

	public void setCuentasPermitidas(Integer cuentasPermitidas) {
		this.cuentasPermitidas = cuentasPermitidas;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public Integer getIdMarcaRol() {
		return idMarcaRol;
	}

	public void setIdMarcaRol(Integer idMarcaRol) {
		this.idMarcaRol = idMarcaRol;
	}
}
