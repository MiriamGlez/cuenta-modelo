package cuentas.back.ccuentas.modelo;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import cuentas.back.general.modelo.Marca;

@Embeddable
public class DetalleMarcaKey implements Serializable{
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;
	
	@ManyToOne
	@JoinColumn(name="MARCAROL_ID")
	private MarcaRol marcaRol;

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public MarcaRol getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(MarcaRol marcaRol) {
		this.marcaRol = marcaRol;
	}
}
