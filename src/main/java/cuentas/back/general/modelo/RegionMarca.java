package cuentas.back.general.modelo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REGION_MARCA", catalog = "GENERAL")
public class RegionMarca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	@JoinColumn(name = "REGION_ID", nullable = false)
	private Region region;

	@Id
	@OneToOne
	@JoinColumn(name = "MARCA_ID", nullable = false)
	private Marca marca;

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region regionId) {
		this.region = regionId;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marcaId) {
		this.marca = marcaId;
	}
}
