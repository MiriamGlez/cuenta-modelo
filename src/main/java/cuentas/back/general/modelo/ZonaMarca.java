package cuentas.back.general.modelo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ZONA_MARCA", catalog = "GENERAL")
public class ZonaMarca implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	@JoinColumn(name = "ZONA_ID", nullable = false)
	private Zona zona;

	@Id
	@OneToOne
	@JoinColumn(name = "MARCA_ID", nullable = false)
	private Marca marca;

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
}
