package cuentas.back.ccuentas.modelo;

import java.io.Serializable;

public class GeneracionCuadros implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String marca;
	private Integer idMarca;
	
	private String marcaRol;
	private Integer idMarcaRol;
	
	private Integer cuadros;
	
	private Integer vueltas;
	
	private String tipoOferta;

	public String getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(String marcaRol) {
		this.marcaRol = marcaRol;
	}

	public Integer getCuadros() {
		return cuadros;
	}

	public void setCuadros(Integer cuadros) {
		this.cuadros = cuadros;
	}

	public Integer getVueltas() {
		return vueltas;
	}

	public void setVueltas(Integer vueltas) {
		this.vueltas = vueltas;
	}

	public String getTipoOferta() {
		return tipoOferta;
	}

	public void setTipoOferta(String tipoOferta) {
		this.tipoOferta = tipoOferta;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public Integer getIdMarcaRol() {
		return idMarcaRol;
	}

	public void setIdMarcaRol(Integer idMarcaRol) {
		this.idMarcaRol = idMarcaRol;
	}
}
