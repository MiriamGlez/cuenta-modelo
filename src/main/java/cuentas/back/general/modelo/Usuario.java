package cuentas.back.general.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USUARIO",catalog = "GENERAL")
public class Usuario implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="USUARIO_ID",unique = true, nullable = false)
	private Integer idUsuario;
	
	@Column(name="CVEUSUARIO")
	private String cveUsuario;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="PATERNO")
	private String paterno;
	
	@Column(name="MATERNO")
	private String materno;
	
	@Column(name = "STATUS")
	private Integer status;
	
	@Column(name = "FECHORACT")
	private Date fecHorAct;
	
	public Usuario() {
	}
	
	public Usuario(Integer idUsuario, String cveUsuario, String nombre, String paterno, String materno, Integer status,
			Date fecHorAct) {
		super();
		this.idUsuario = idUsuario;
		this.cveUsuario = cveUsuario;
		this.nombre = nombre;
		this.paterno = paterno;
		this.materno = materno;
		this.status = status;
		this.fecHorAct = fecHorAct;
	}


	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getCveUsuario() {
		return cveUsuario;
	}
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}
}
