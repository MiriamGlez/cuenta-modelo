package cuentas.back.general.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VIA", catalog="GENERAL")
public class Via implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="VIA_ID")
	private Integer id;
	
	@Column(name="NOMBVIA")
	private String nombVia;
	
	@Column(name="FECHORACT")
	private Date fecHorAct;
	
	@Column(name="STATUS")
	private Integer status;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId;
	
//	@Column(name="AUTORIZADO")
//	private Integer autorizado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombVia() {
		return nombVia;
	}

	public void setNombVia(String nombVia) {
		this.nombVia = nombVia;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

//	public Integer getAutorizado() {
//		return autorizado;
//	}
//
//	public void setAutorizado(Integer autorizado) {
//		this.autorizado = autorizado;
//	}
}