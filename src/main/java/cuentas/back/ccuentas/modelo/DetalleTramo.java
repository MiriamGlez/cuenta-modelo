package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name ="DETALLE_TRAMO", catalog ="CCUENTAS")
public class DetalleTramo implements Serializable{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetalleTramoKey key = null;
	
	@Column(name="FIN")
	private Timestamp fin = null;
	
	@Column(name="TRECORRIDO")
	private Timestamp tRecorrido = null;
	
	@Column(name="FECHORACT")
	private Date fecHorAct = null;
	
	@Column(name="USUARIO_ID")
	private Integer usuarioId = null;

	public DetalleTramoKey getKey() {
		return key;
	}

	public void setKey(DetalleTramoKey key) {
		this.key = key;
	}

	public Timestamp getFin() {
		return fin;
	}

	public void setFin(Timestamp fin) {
		this.fin = fin;
	}

	public Timestamp gettRecorrido() {
		return tRecorrido;
	}

	public void settRecorrido(Timestamp tRecorrido) {
		this.tRecorrido = tRecorrido;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
}
