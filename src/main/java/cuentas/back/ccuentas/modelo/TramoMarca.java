package cuentas.back.ccuentas.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Via;

@Entity
@Table(name = "TRAMOS_MARCA", catalog  = "CCUENTAS")
public class TramoMarca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRAMOSMARCA_ID")
	private Integer tramoMarcaId;
	
	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;

	@ManyToOne
	@JoinColumn(name="ORIGEN_ID")
	private Parada origen;

	@ManyToOne
	@JoinColumn(name="DESTINO_ID")
	private Parada destino;

	@ManyToOne
	@JoinColumn(name="VIA_ID")
	private Via via;

	@Column(name = "KMS_RECORRIDOS")
	private Integer kmsRecorridos;
	
	@Column(name = "FECHORACT")
	private Date fecHorAct;

	@Column(name = "USUARIO_ID")
	private Integer usuarioId;

	@Column(name = "ESTATUS")
	private Integer estatus;

	@Column(name = "CON_PISTA")
	private Integer conPista;

	@Column(name = "COSTO")
	private Double costo;

	public Integer getKmsRecorridos() {
		return kmsRecorridos;
	}

	@Transient
	private Date tRecorrido;
	
	@Transient
	private boolean selecPista;
	
	public void setKmsRecorridos(Integer kmsRecorridos) {
		this.kmsRecorridos = kmsRecorridos;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFecHorAct() {
		return fecHorAct;
	}

	public void setFecHorAct(Date fecHorAct) {
		this.fecHorAct = fecHorAct;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getConPista() {
		return conPista;
	}

	public void setConPista(Integer conPista) {
		this.conPista = conPista;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public Integer getTramoMarcaId() {
		return tramoMarcaId;
	}

	public void setTramoMarcaId(Integer tramoMarcaId) {
		this.tramoMarcaId = tramoMarcaId;
	}

	public Date gettRecorrido() {
		return tRecorrido;
	}

	public void settRecorrido(Date tRecorrido) {
		this.tRecorrido = tRecorrido;
	}

	public boolean isSelecPista() {
		return selecPista;
	}

	public void setSelecPista(boolean selecPista) {
		this.selecPista = selecPista;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Parada getOrigen() {
		return origen;
	}

	public void setOrigen(Parada origen) {
		this.origen = origen;
	}

	public Parada getDestino() {
		return destino;
	}
	public void setDestino(Parada destino) {
		this.destino = destino;
	}
	public Via getVia() {
		return via;
	}
	public void setVia(Via via) {
		this.via = via;
	}
}
