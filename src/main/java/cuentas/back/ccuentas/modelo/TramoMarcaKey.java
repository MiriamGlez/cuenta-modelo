package cuentas.back.ccuentas.modelo;

import java.io.Serializable;


import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Via;

@Embeddable
public class TramoMarcaKey implements Serializable{

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="MARCA_ID")
	private Marca marca;

	@ManyToOne
	@JoinColumn(name="ORIGEN_ID")
	private Parada origen;

	@ManyToOne
	@JoinColumn(name="DESTINO_ID")
	private Parada destino;

	@ManyToOne
	@JoinColumn(name="VIA_ID")
	private Via via;
	
	public Parada getOrigen() {
		return origen;
	}

	public void setOrigen(Parada origen) {
		this.origen = origen;
	}

	public Parada getDestino() {
		return destino;
	}

	public void setDestino(Parada destino) {
		this.destino = destino;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}
}
